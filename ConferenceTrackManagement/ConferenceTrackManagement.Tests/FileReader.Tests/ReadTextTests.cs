﻿using ConferenceTrackManagement.Common;
using ConferenceTrackManagement.FileReader;

namespace ConferenceTrackManagement.Tests.FileReader.Tests
{
    using System.IO;
    using NUnit.Framework;


    [TestFixture]
    public class ReadTextTests
    {
        private ReadText readText; //Real world solutions have moq in place.

        [SetUp]
        public void SetUp()
        {
            readText=new ReadText();
        }
        [Test]
        public void Verify_file_path_to_return_values()
        {
            //Arrange - Act - Assert

            //write these test cases first, call the read method, test will fail, refactor the code to make it pass.

            string filepath = FilePath.filePath();

            var result=readText.Read(filepath);

            Assert.IsNotEmpty(result);
        }
    }
}
