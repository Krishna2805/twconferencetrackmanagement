﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConferenceTrackManagement.ConferenceScheduler;
using ConferenceTrackManagement.Model;
using NUnit.Framework;

namespace ConferenceTrackManagement.Tests.ConferenceScheduler.Tests
{
    public class ConferenceTrackSchedulerTests
    {
        private ConferenceTrackSchedulerFactory factory; //Real world solutions have moq in place.

        [SetUp]
        public void SetUp()
        {
            factory = new ConferenceTrackSchedulerFactory();
        }
        [Test]
        public void Verify_file_path_to_return_values_duration()
        {
            //Arrange - Act - Assert

            //write these test cases first, call the read method, test will fail, refactor the code to make it pass.

            var input = new List<Talk>() { new Talk(){TalkStartTime = TimeSpan.FromHours(9),Duration = TimeSpan.FromMinutes(60),TalkName ="User Interface CSS in Rails Apps" },new Talk(){TalkStartTime = TimeSpan.FromHours(10),Duration = TimeSpan.FromMinutes(60),TalkName ="User Interface CSS in Rails Apps"} };
            var fact=factory.GenerateTalks(true);
            var result = fact.GenerateTrackTalks(TimeSpan.FromMinutes(90), input, TimeSpan.FromHours(9));
            Assert.AreEqual(TimeSpan.FromMinutes(60), result.First().Duration);
        }

        [Test]
        public void Verify_file_path_to_return_values_talkName()
        {
            var input = new List<Talk>() { new Talk() { TalkStartTime = TimeSpan.FromHours(9), Duration = TimeSpan.FromMinutes(60), TalkName = "User Interface CSS in Rails Apps" }, new Talk() { TalkStartTime = TimeSpan.FromHours(10), Duration = TimeSpan.FromMinutes(60), TalkName = "User Interface CSS in Rails Apps" } };
            var fact = factory.GenerateTalks(true);
            var result = fact.GenerateTrackTalks(TimeSpan.FromMinutes(90), input, TimeSpan.FromHours(9));
            Assert.AreEqual("User Interface CSS in Rails Apps", result.First().TalkName);
        }

        [Test]
        public void Verify_file_path_to_return_values_Duration()
        {
            var input = new List<Talk>() { new Talk() { TalkStartTime = TimeSpan.FromHours(9), Duration = TimeSpan.FromMinutes(60), TalkName = "User Interface CSS in Rails Apps" }, new Talk() { TalkStartTime = TimeSpan.FromHours(10), Duration = TimeSpan.FromMinutes(60), TalkName = "User Interface CSS in Rails Apps" } };
            var fact = factory.GenerateTalks(true);
            var result = fact.GenerateTrackTalks(TimeSpan.FromMinutes(90), input, TimeSpan.FromHours(9));
            Assert.AreEqual(TimeSpan.FromMinutes(60), result.First().Duration);
        }

        [Test]
        public void Verify_file_path_to_return_values_Count()
        {
            var input = new List<Talk>() { new Talk() { TalkStartTime = TimeSpan.FromHours(9), Duration = TimeSpan.FromMinutes(60), TalkName = "User Interface CSS in Rails Apps" } };
            var fact = factory.GenerateTalks(true);
            var result = fact.GenerateTrackTalks(TimeSpan.FromMinutes(180), input, TimeSpan.FromHours(9));
            Assert.AreEqual(1, result.Count);
        }
    }
}
