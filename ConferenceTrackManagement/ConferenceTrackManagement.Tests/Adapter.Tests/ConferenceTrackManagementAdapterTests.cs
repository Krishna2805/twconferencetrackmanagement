﻿using ConferenceTrackManagement.Adapter;

namespace ConferenceTrackManagement.Tests.Adapter.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;

    public class ConferenceTrackManagementAdapterTests
    {

        private ConferenceTrackManagementAdapter adapter; //Real world solutions have moq in place.

        [SetUp]
        public void SetUp()
        {
            adapter = new ConferenceTrackManagementAdapter();
        }
        [Test]
        public void Verify_file_path_to_return_values_duration()
        {
            //Arrange - Act - Assert

            //write these test cases first, call the read method, test will fail, refactor the code to make it pass.

            var input = new List<string>() { "User Interface CSS in Rails Apps 30min", "User Interface JS in Rails Apps 60min" };
            adapter.Connect(input);
            var result = adapter.Convert();
            Assert.AreEqual(TimeSpan.FromMinutes(30),result.First().Duration);
        }

        [Test]
        public void Verify_file_path_to_return_values_talkName()
        {
            //Arrange - Act - Assert

            //write these test cases first, call the read method, test will fail, refactor the code to make it pass.

            var input = new List<string>() { "User Interface CSS in Rails Apps 30min", "User Interface JS in Rails Apps 60min" };
            adapter.Connect(input);
            var result = adapter.Convert();
            Assert.AreEqual("User Interface CSS in Rails Apps ", result.First().TalkName);
        }
    }
}
