﻿using ConferenceTrackManagement.Adapter;
using ConferenceTrackManagement.ConferenceScheduler;
using ConferenceTrackManagement.FileReader;
using ConferenceTrackManagement.Model;

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ConferenceTrackManagement
    {

        public readonly IReadText _readText;
        public readonly ConferenceTrackManagementAdapter ObjConferenceTrackManagementAdapter = new ConferenceTrackManagementAdapter();
        public readonly ConferenceTrackSchedulerFactory ConferenceTrackSchedulerFactory = new ConferenceTrackSchedulerFactory();
        public List<string> TalkInputList=new List<string>();
        public double TotalDays;
        public List<Talk> TalkInput;
        public TimeSpan MorningTalkEndingHour;
        public TimeSpan AfternoonTalkEndingHour;
        public IConferenceTrackSchedulerFactory ObjMorningTalkList;
        public IConferenceTrackSchedulerFactory ObjAfternoonTalkList;
        public List<Talk> MorningTalkList;
        public List<Talk> AfternoonTalkList;
        public readonly Schedular _schedular;


        //Interface assignment for loose coupling - constructor based DI
        public ConferenceTrackManagement(IReadText readText)
        {
            this._readText = readText;
            _schedular = new Schedular(this);
        }

        public Schedular Schedular
        {
            get { return _schedular; }
        }

        public static void GenerateOutput(List<Talk> morningTalkList, List<Talk> afternoonTalkList)
        {
            foreach (var talk in morningTalkList)
            {
                Console.WriteLine("{0} {1} {2}", talk.TalkStartTime, talk.TalkName, talk.Duration);
            }

            Console.WriteLine("{0} {1}", TimeSpan.FromHours(12), "Lunch");

            foreach (var talk in afternoonTalkList)
            {
                Console.WriteLine("{0} {1} {2}", talk.TalkStartTime, talk.TalkName, talk.Duration);
            }

            Console.WriteLine("{0} {1}", TimeSpan.FromHours(17), "Networking");
        }

        public static List<Talk> SortUnsortedTalkInput(List<Talk> unSortedTalkInput, out double totalDays)
        {
            var talkInput =
                unSortedTalkInput.OrderByDescending(input => input.Duration)
                    .Select(
                        input =>
                            new Talk()
                            {
                                TalkStartTime = input.TalkStartTime,
                                TalkName = input.TalkName,
                                Duration = input.Duration
                            }).ToList();
            double totalDuration = talkInput.Sum(talk => talk.Duration.TotalHours);

            totalDays = (double) totalDuration/6;
            return talkInput;
        }


        static void Main(string[] args)
        {
            var objCconferenceTrackManagement = new ConferenceTrackManagement(new ReadText());
            objCconferenceTrackManagement.Schedular.ConferenceScheduler(args[0]);
        }
    }
}
