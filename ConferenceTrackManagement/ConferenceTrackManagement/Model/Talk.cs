﻿namespace ConferenceTrackManagement.Model
{
    using System;

    public class Talk
    {
        public TimeSpan TalkStartTime { get; set; }

        public string TalkName { get; set; }

        public TimeSpan Duration { get; set; }
    }
}
