﻿namespace ConferenceTrackManagement.FileReader
{
    using System.Collections.Generic;

    public interface IReadText
    {
        List<string> Read(string path);
    }
}
