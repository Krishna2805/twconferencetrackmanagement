﻿namespace ConferenceTrackManagement.FileReader
{
    using System;
    using System.Collections.Generic;
    using System.IO;


    public class ReadText : IReadText
    {
        public List<string> Read(string path)
        {
            var stringLine = new List<string>();
            int lineNumber = 0;
            try
            {
                using (var sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (Validations.Validate(line, lineNumber)) stringLine.Add(line);
                        lineNumber++;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return stringLine;

        }

    }
}
