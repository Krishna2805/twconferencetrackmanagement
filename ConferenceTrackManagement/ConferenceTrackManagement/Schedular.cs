using System;
using System.Collections.Generic;
using ConferenceTrackManagement.Model;

namespace ConferenceTrackManagement
{
    public class Schedular
    {
        private ConferenceTrackManagement _conferenceTrackManagement;

        public Schedular(ConferenceTrackManagement conferenceTrackManagement)
        {
            _conferenceTrackManagement = conferenceTrackManagement;
        }

        public void ConferenceScheduler(string filePath)
        {
            _conferenceTrackManagement.TalkInputList = _conferenceTrackManagement._readText.Read(filePath);

            var talkInput = _conferenceTrackManagement.ObjConferenceTrackManagementAdapter.Connect(_conferenceTrackManagement.TalkInputList);

            GenerateSchedule(talkInput);

            Console.ReadLine();
        }

        private void GenerateSchedule(List<Talk> unSortedTalkInput)
        {
            _conferenceTrackManagement.TalkInput = ConferenceTrackManagement.SortUnsortedTalkInput(unSortedTalkInput, out _conferenceTrackManagement.TotalDays);

            _conferenceTrackManagement.MorningTalkEndingHour = TimeSpan.FromMinutes(720);
            //12*60 - Ending time for morning is 12 and converted to minutes
            _conferenceTrackManagement.AfternoonTalkEndingHour = TimeSpan.FromMinutes(1020);
            //17*60 - Ending Time for evening is 5pm and converted to minutes

            for (double d = 1.0; d <= _conferenceTrackManagement.TotalDays; d++)
            {
                Console.WriteLine("TRACK {0}", d);
                _conferenceTrackManagement.ObjMorningTalkList = _conferenceTrackManagement.ConferenceTrackSchedulerFactory.GenerateTalks(true);
                _conferenceTrackManagement.MorningTalkList = _conferenceTrackManagement.ObjMorningTalkList.GenerateTrackTalks(_conferenceTrackManagement.MorningTalkEndingHour, _conferenceTrackManagement.TalkInput, TimeSpan.FromHours(9));
                RemoveScheduled(_conferenceTrackManagement.TalkInput, _conferenceTrackManagement.MorningTalkList);
                _conferenceTrackManagement.ObjAfternoonTalkList = _conferenceTrackManagement.ConferenceTrackSchedulerFactory.GenerateTalks(false);
                _conferenceTrackManagement.AfternoonTalkList = _conferenceTrackManagement.ObjAfternoonTalkList.GenerateTrackTalks(_conferenceTrackManagement.AfternoonTalkEndingHour, _conferenceTrackManagement.TalkInput, TimeSpan.FromHours(13));
                RemoveScheduled(_conferenceTrackManagement.TalkInput, _conferenceTrackManagement.AfternoonTalkList);

                ConferenceTrackManagement.GenerateOutput(_conferenceTrackManagement.MorningTalkList, _conferenceTrackManagement.AfternoonTalkList);
            }
        }

        public List<Talk> RemoveScheduled(List<Talk> talkInput, List<Talk> morningTalks)
        {
            foreach (var morningTalk in morningTalks)
            {
                talkInput.RemoveAll(talk => talk.TalkName == morningTalk.TalkName);
            }
            return talkInput;
        }
    }
}