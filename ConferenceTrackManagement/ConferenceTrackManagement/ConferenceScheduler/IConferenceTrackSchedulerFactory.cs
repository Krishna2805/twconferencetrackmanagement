﻿using ConferenceTrackManagement.Model;

namespace ConferenceTrackManagement.ConferenceScheduler
{
    using System;
    using System.Collections.Generic;

    public interface IConferenceTrackSchedulerFactory
    {
        List<Talk> GenerateTrackTalks(TimeSpan talkHours, List<Talk> talkInput, TimeSpan timeSpan);
    }
}
