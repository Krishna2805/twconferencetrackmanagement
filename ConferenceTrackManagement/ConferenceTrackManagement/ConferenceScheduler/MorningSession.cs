﻿using ConferenceTrackManagement.Model;

namespace ConferenceTrackManagement.ConferenceScheduler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
   
    public class MorningSession : IConferenceTrackSchedulerFactory
    {
        public List<Model.Talk> GenerateTrackTalks(TimeSpan talkHours, List<Model.Talk> talkInput, TimeSpan timeSpan)
        {
            var talks = talkInput.ToArray();
            var talking = new List<Talk>();
            var result = TimeSpan.Zero;

            for (int i = 0; i < talks.Length; i++)
            {

                if (i == 0) { talks[0].TalkStartTime = timeSpan; talking.Add(new Talk() { TalkStartTime = talks[0].TalkStartTime, TalkName = talks[0].TalkName, Duration = talks[0].Duration }); }
                if (i != 0 && result!=talkHours)
                {
                    if (talking[talking.Count - 1].TalkStartTime + talking[talking.Count - 1].Duration + talks[i].Duration <= talkHours)
                        {

                            talks[i].TalkStartTime = talking[talking.Count - 1].TalkStartTime + talking[talking.Count - 1].Duration;
                            if (talks[i].TalkStartTime != talkHours)
                            { 
                            talking.Add(new Talk() { TalkStartTime = talks[i].TalkStartTime, TalkName = talks[i].TalkName, Duration = talks[i].Duration });
                                result = talking[talking.Count - 1].TalkStartTime;
                            }
                    }
                }

            }
            return talking.ToList();
        }
    }
}
