﻿namespace ConferenceTrackManagement.ConferenceScheduler
{
    public class ConferenceTrackSchedulerFactory
    {
        private IConferenceTrackSchedulerFactory _generatedTalks;

        public IConferenceTrackSchedulerFactory GenerateTalks(bool value)
        {
            switch (value)
            {
                case true:
                    _generatedTalks = new MorningSession();
                    break;
                case false:
                    _generatedTalks = new AfternoonSession();
                    break;
            }
            return _generatedTalks;
        }

    }
}
