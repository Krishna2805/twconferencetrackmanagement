﻿using ConferenceTrackManagement.Model;

namespace ConferenceTrackManagement.Adapter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ConferenceTrackManagementAdapter : IConferenceTrackManagementAdapter
    {
        readonly List<Talk> _talkInput = new List<Talk>();
        public List<Talk> Convert()
        {
            return _talkInput;
        }

        public List<Talk> Connect(List<string> inputTalk)
        {
            foreach (var inputTask in inputTalk)
            {

                string durationWithMin = inputTask.Split(' ').Last();
                string duration = durationWithMin.Replace("min", "");
                string talkName = inputTask.Replace(durationWithMin, "");
                if (durationWithMin.Contains("lightning"))
                {
                    duration = "5";
                }

                _talkInput.Add(new Talk() { TalkName = talkName, Duration = duration.ToTimeSpan() });

            }
            return _talkInput;
        }
    }

    public static class TimeSpanConversion
    {
        public static TimeSpan ToTimeSpan(this string duration)
        {
            return TimeSpan.FromMinutes(Convert.ToDouble(duration));
        }

    }

}
