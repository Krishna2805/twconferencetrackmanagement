﻿using ConferenceTrackManagement.Model;

namespace ConferenceTrackManagement.Adapter
{
    using System.Collections.Generic;

    public interface IConferenceTrackManagementAdapter
    {
        List<Talk> Convert();
    }
}
